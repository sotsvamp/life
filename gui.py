import curses
from world import World

#http://stackoverflow.com/questions/566746/how-to-get-console-window-width-in-python
def getTerminalSize():
    import os
    env = os.environ
    def ioctl_GWINSZ(fd):
        try:
            import fcntl, termios, struct, os
            cr = struct.unpack('hh', fcntl.ioctl(fd, termios.TIOCGWINSZ,
        '1234'))
        except:
            return
        return cr
    cr = ioctl_GWINSZ(0) or ioctl_GWINSZ(1) or ioctl_GWINSZ(2)
    if not cr:
        try:
            fd = os.open(os.ctermid(), os.O_RDONLY)
            cr = ioctl_GWINSZ(fd)
            os.close(fd)
        except:
            pass
    if not cr:
        cr = (env.get('LINES', 25), env.get('COLUMNS', 80))

        ### Use get(key[, default]) instead of a try/catch
        #try:
        #    cr = (env['LINES'], env['COLUMNS'])
        #except:
        #    cr = (25, 80)
    return int(cr[1]), int(cr[0])

class conwayGUI:
    def __init__(self, stdscreen):
        stdscreen.clear()
        curses.curs_set(0)
        stdscreen.border()
        stdscreen.refresh()
        
        self.update_size()

        self.window = stdscreen

    def update_size(self):
        self.window_size_x, self.window_size_y = getTerminalSize()
        self.window_size_y -= 2 # Räknar bort ramen från spelområdet
        self.window_size_x -= 2 # Räknar bort ramen från spelområdet

    def get_window_x(self):
        return self.window_size_x

    def get_window_y(self):
        return self.window_size_y


    def update_game(self, world, population = None):
        """
        updates the screen between game rounds
        """

        while True:
            self.update_size()
            self.window.resize(self.window_size_y+2, self.window_size_x+2)
            world.resize(self.window_size_x, self.window_size_y)
            self.window.clear()
            self.window.border()
            world.resize(self.window_size_x, self.window_size_y)
            for y in range(world.num_rows):
                for x in range(world.num_cols):
                    if world.is_alive(x, y):
                        self.window.addstr(y+1, x+1, '*') 
            if population != None:
                pop_string = "Population: {}".format(population)
                if self.window_size_x >= len(pop_string):
                    self.window.addstr(0, self.window_size_x - len(pop_string) + 1, pop_string)
            if self.window_size_x >= 26:
                self.window.addstr(self.window_size_y+1, self.window_size_x - 26, 'Push any button to continue')
            self.window.refresh()
            key = self.window.getch()
            if key != curses.KEY_RESIZE:
                if key == ord('q'):
                    quit()
                break
        


