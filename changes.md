## Varför jag vill bygga om life-repot

+ Jag tycker att det är lite väl abstrakt i dag. Det är en stor värld som liksom hoppar rundor på skärmen och man ser aldrig en kant.

+ Genom att begränsa spelplanen till terminalfönstret är det tydligare för eleverna att det faktiskt är en ändlig värld och det behövs regler för att hantera kanterna.

+ Jag tror att det går att snabba upp programmet.



## Hur jag tänker mig att det ska vara uppbyggt

Tre filer i repot, world.py, ui.py och life.py.

### world.py

+ En klass som är en matris med alla celler och en del funktioner
	- class World(cols, rows)
		- def populate(pattern=['xx x', ' x x', ' xx '])
		- def give_life(x, y)
		- def take_life(x, y)
		- def is_alive(x, y)
		- def get_no_of_columns()
		- def get_no_of_rows()
		
+ Vid skapandet av ett World-objekt anges storleken på världen och den är alltid tom. Funktionen populate() har ett valfritt inargument som senare i övningen ska vara användarinmatat.

### ui.py

+ Här sköts allt med utskrift på skärmen. Huruvida detta bör/ska/måste vara en klass eller bara ett gäng funktioner vet jag inte, men det är förhoppningsvis självklart när flödet är beskrivet. Funktioner som ska finnas är
	- def init_terminal()
		return World(x, y).populate()
	- def print_world(world)


### life.py

+ Det är här eleverna skriver all sin kod. När den kommer är den tom så när som på några få rader som initierar terminalen och skapar en exempelvärld så att de direkt kan köra filen och se hur världen kommer att se ut.

+ Det är här alla regler och programflödet ska skapas av eleverna enligt mallen som finns med alla de delfunktioner och tester som redan finns i `lifetest.py`.



## Flödet

Så här tänker jag mig att filerna ska fungera i programflödet.

	# life.py
	
	def main()
		# terminalfönstret initieras och sen returnera en värld med rätt storlek
		# och med en default-population.
		my_world = ui.init_terminal()
		
		# print_world skriver ut världen i inargumentet på skärmen
		ui.print_world(my_world)
		
Ovanstående är vad som ges i filen från början, men efterhand som eleverna jobbar sig fram i materialet så ändras flödet till att bli något i stil med

	def main()
		my_world = ui.init_terminal()
		
		while True:
			ui.print_world(my_world)
			
			# update_world skrivs i slutet av övningen och är den funktion som sätter ihop
			# alla tidigare skrivna funktioner, applicerar regler etc
			my_world = update_world(my_world)

Och det är allt som behövs i `main()`, resten finns i de funktioner som vårt material säger åt dem att skriva. Komplett med tester längs vägen så att det går ihop i slutet när `update_world()` ska skrivas.


## Extrauppgifter

I den första extrauppgiften ska man skapa en inmatningsrutin för att kunna göra egna startmönster. Efter att inmatningsfunktionen är skriven ska mönstret skickas till World.populate(), så inga konstigheter här.

I nästa extrauppgift ska man räkna antal invånare i världen och skicka med till utskriften. Det kan inte vara svårare att lägga till i den här lösningen jämfört med existerande lösning.

Samma sak med sista extrauppgiften som jämför nuvarande värld med nästa värld och det är ingen skillnad i implementering här heller.



## Extra extrauppgifter

Med curses kommer många fler funktioner som skapar möjligheter till många extra extrauppgifter för elever som inte vill ha någon fritid.

+ Använda pads i curses som kan öka storleken på världen långt utanför terminalfönsterstorleken.
+ Det finns färdig tangentbordshantering som gör det möjligt att göra inmatning av en ny startpopulation med hjälp av tangenterna.



## Problem

+ Om fönstret resizas till det mindre, alternativt textstorleken ökas, under programkörning så dör allting. Det är ett problem, men ett litet sådant eftersom de flesta aldrig resizear ett fönster. Det kan så klart lösas, men jag vet inte hur i nuläget.

+ Världen blir mindre än nuvarande lösning, men instruktioner kan ges där eleverna ändrar storlek på terminalfönstret och minskar textstorleken innan de startar programmet och då blir världen större.

